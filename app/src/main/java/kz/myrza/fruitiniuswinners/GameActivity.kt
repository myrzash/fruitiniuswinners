package kz.myrza.fruitiniuswinners

import android.app.Activity
import android.app.AlertDialog
import android.content.res.Configuration
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_game.*
import kotlinx.android.synthetic.main.dialog_result.view.*
import kotlinx.android.synthetic.main.item_fruit.view.*
import kz.myrza.fruitiniuswinners.adapter.FruitService
import kz.myrza.fruitiniuswinners.adapter.FruitsAdapter
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledFuture
import java.util.concurrent.TimeUnit


class GameActivity : Activity(), FruitService {

    private var timerExecutor: ScheduledFuture<*>? = null
    private val defaultImage = "seven"
    private var currentLevel = 1
    private var timeInSeconds: Long = 0
    private lateinit var listAdapter: FruitsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)

        initRecyclerView()

        restartGame()

        setLevel()

        floatingActionButton.setOnClickListener { restartGame() }

        textViewTimer.text = "00:00"
    }

    //    TODO: FruitService override functions
//    ------------------------------------------------------
    override fun selectItem(view: View, fruitImageName: String) {
        view.scaleX = 0.9f
        view.scaleY = 0.9f
        view.isEnabled = false
        val resId = getResIdImage(fruitImageName)
        view.fruitImageView.setImageResource(resId)
        setAnimation(view)
    }

    override fun unSelectItem(view: View) {
        view.scaleX = 1f
        view.scaleY = 1f
        view.isEnabled = true
        view.alpha = 1f
        val resId = getResIdImage(defaultImage)
        view.fruitImageView.setImageResource(resId)
    }

    override fun correctItem(view: View) {
        view.scaleX = 1f
        view.scaleY = 1f
        view.alpha = 0.8f
    }

    override fun whenGameOver(wrongAttempts: Int) {
        timerExecutor?.cancel(true)
        var score = 100 - wrongAttempts * 2.5
        if (score < 0) score = 0.0

        val dialogView = layoutInflater.inflate(R.layout.dialog_result, null)
        dialogView.textViewScore.text = getString(R.string.dialog_score, "$score%")
        dialogView.textViewTime.text =
            getString(R.string.dialog_time, getFormattedTime(timeInSeconds))

        val builder = AlertDialog.Builder(this)
            .setCancelable(false)
            .setView(dialogView)
            .show()

        dialogView.buttonRepeat.setOnClickListener {
            restartGame()

            currentLevel++
            setLevel()

            builder.dismiss()
        }
    }

    //    TODO: Private inner functions
//    ------------------------------------------------------
    private fun initRecyclerView () {
        val heightScreen = windowManager.defaultDisplay.height / 9 * 5
        var columnCount = 4
        var heightCard = heightScreen / 3

        if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            columnCount = 3
            heightCard = heightScreen / 4
        }

        listAdapter = FruitsAdapter(this, heightCard)
        val gridLayoutManager = GridLayoutManager(this, columnCount)

        recyclerViewGame.apply {
            layoutManager = gridLayoutManager
            adapter = listAdapter
        }
    }

    private fun getResIdImage(imageName: String): Int {
        return resources.getIdentifier(
            imageName,
            "drawable",
            packageName
        )
    }

    private fun getFormattedTime(timeInSeconds: Long): String {
        val minutes = TimeUnit.SECONDS.toMinutes(timeInSeconds) % 60
        val seconds = timeInSeconds % 60//  - minutes * 60
        return String.format("%02d:%02d", minutes, seconds)
    }

    private fun restartGame() {
        timeInSeconds = 0
        resumeTimer()
        listAdapter.reload(getRandomImages())
    }

    private fun getRandomImages(): MutableList<String> {
        val images = resources.getStringArray(R.array.fruit_images).toMutableList()
        images.shuffle()
        images.add(images[0])

        val randomImages = images.map {
            listOf(it, it)
        }.flatten().toMutableList()
        randomImages.shuffle()

        return randomImages
    }

    private fun resumeTimer() {
        timerExecutor?.cancel(true)
        timerExecutor = Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate({
            timeInSeconds++
            runOnUiThread {
                textViewTimer.text = getFormattedTime(timeInSeconds)
            }
        }, 1, 1, TimeUnit.SECONDS)
    }

    private fun setLevel() {
        textViewLevel.text = "$currentLevel"
    }

    private fun setAnimation(viewToAnimate: View) {
        val animation: Animation = AnimationUtils.loadAnimation(this, R.anim.card_select_animation)
        viewToAnimate.startAnimation(animation)
    }

    //    TODO: Activity Life Cycle
//    ------------------------------------------------------
    override fun onResume() {
        super.onResume()
        resumeTimer()
    }

    override fun onPause() {
        super.onPause()
        timerExecutor?.cancel(true)
    }

}
