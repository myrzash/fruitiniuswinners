package kz.myrza.fruitiniuswinners.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView

class FruitViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(position: Int) {
        itemView.tag = position
    }
}