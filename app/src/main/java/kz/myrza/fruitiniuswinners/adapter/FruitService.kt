package kz.myrza.fruitiniuswinners.adapter

import android.view.View

interface FruitService {
    fun selectItem(view: View, fruitImageName: String)

    fun unSelectItem(view: View)

    fun correctItem(view: View)

    fun whenGameOver(wrongAttempts: Int)
}