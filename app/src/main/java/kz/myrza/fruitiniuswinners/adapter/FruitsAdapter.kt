package kz.myrza.fruitiniuswinners.adapter

import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kz.myrza.fruitiniuswinners.R

class FruitsAdapter(private val activity: FruitService, private val heightCard: Int) :
    RecyclerView.Adapter<FruitViewHolder>(), View.OnClickListener {

    private var images = mutableListOf<String>()
    private var isActiveOnClick = true
    private var lastView: View? = null
    private var corrects = 0
    private var wrongAttempts = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FruitViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.item_fruit, parent, false)
        view.layoutParams.height = heightCard // parent.measuredHeight/6 * 5
        view.setOnClickListener(this)

        return FruitViewHolder(view)
    }

    override fun getItemCount(): Int = images.size

    override fun onBindViewHolder(holder: FruitViewHolder, position: Int) {
        holder.bind(position)
        activity.unSelectItem(holder.itemView)
    }

    override fun onClick(view: View) {

        if (!isActiveOnClick) return

        val position = view.tag as Int
        if (position == RecyclerView.NO_POSITION) return
        activity.selectItem(view, images[position])

        if (lastView !== null) {
            val lastPosition = lastView!!.tag as Int

            val isCorrect = images[lastPosition] == images[position]

            if (isCorrect) {
                activity.correctItem(view)
                activity.correctItem(lastView!!)
                lastView = null
                corrects++

                if (corrects == images.size / 2) {
                    activity.whenGameOver(wrongAttempts)
                }

            } else {
                wrongAttempts++
                isActiveOnClick = false
                Handler().postDelayed({
                    activity.unSelectItem(view)
                    activity.unSelectItem(lastView!!)
                    lastView = null
                    isActiveOnClick = true
                }, 800)
            }

        } else {
            lastView = view
        }
    }

    fun reload(images: MutableList<String>) {
        this.isActiveOnClick = true
        this.images = images
        this.lastView = null
        this.corrects = 0
        this.wrongAttempts = 0
        notifyDataSetChanged()
    }
}